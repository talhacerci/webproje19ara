﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProgramlamaMatematikciyiz.Models;

namespace WebProgramlamaMatematikciyiz.Controllers
{
    public class KonuVideosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [Authorize]
        // GET: KonuVideos
        public async Task<ActionResult> Index()
        {
            var konuAnlatimVideos = db.KonuAnlatimVideos.Include(k => k.Konulars);
            return View(await konuAnlatimVideos.ToListAsync());
        }

        // GET: KonuVideos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuVideo konuAnlatimVideo = await db.KonuAnlatimVideos.FindAsync(id);
            db.KonuAnlatimVideos.Include(x => x.Konulars).Include(x => x.Konulars.Dersler).ToList();
            ViewBag.link = konuAnlatimVideo.Link;

            if (konuAnlatimVideo == null)
            {
                return HttpNotFound();
            }
            return View(konuAnlatimVideo);
        }

        // GET: KonuVideos/Create
        public ActionResult Create()
        {
            ViewBag.KonuID = new SelectList(db.KonuAnlatims, "KonuID", "KonuAdi");
            return View();
        }

        // POST: KonuVideos/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "KonuVideoID,Link,KonuID")] KonuVideo konuVideo)
        {
            if (ModelState.IsValid)
            {
                db.KonuAnlatimVideos.Add(konuVideo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.KonuID = new SelectList(db.KonuAnlatims, "KonuID", "KonuAdi", konuVideo.KonuID);
            return View(konuVideo);
        }

        // GET: KonuVideos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuVideo konuVideo = await db.KonuAnlatimVideos.FindAsync(id);
            if (konuVideo == null)
            {
                return HttpNotFound();
            }
            ViewBag.KonuID = new SelectList(db.KonuAnlatims, "KonuID", "KonuAdi", konuVideo.KonuID);
            return View(konuVideo);
        }

        // POST: KonuVideos/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "KonuVideoID,Link,KonuID")] KonuVideo konuVideo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(konuVideo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.KonuID = new SelectList(db.KonuAnlatims, "KonuID", "KonuAdi", konuVideo.KonuID);
            return View(konuVideo);
        }

        // GET: KonuVideos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuVideo konuVideo = await db.KonuAnlatimVideos.FindAsync(id);
            if (konuVideo == null)
            {
                return HttpNotFound();
            }
            return View(konuVideo);
        }

        // POST: KonuVideos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            KonuVideo konuVideo = await db.KonuAnlatimVideos.FindAsync(id);
            db.KonuAnlatimVideos.Remove(konuVideo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
