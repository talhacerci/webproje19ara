namespace WebProgramlamaMatematikciyiz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dersler : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dersler",
                c => new
                    {
                        DersID = c.Int(nullable: false, identity: true),
                        DersAdi = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.DersID);
            
            CreateTable(
                "dbo.Konular",
                c => new
                    {
                        KonuID = c.Int(nullable: false, identity: true),
                        KonuAdi = c.String(nullable: false, maxLength: 200),
                        DersID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KonuID)
                .ForeignKey("dbo.Dersler", t => t.DersID, cascadeDelete: true)
                .Index(t => t.DersID);
            
            CreateTable(
                "dbo.KonuVideo",
                c => new
                    {
                        KonuVideoID = c.Int(nullable: false, identity: true),
                        Link = c.String(nullable: false),
                        KonuID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KonuVideoID)
                .ForeignKey("dbo.Konular", t => t.KonuID, cascadeDelete: true)
                .Index(t => t.KonuID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KonuVideo", "KonuID", "dbo.Konular");
            DropForeignKey("dbo.Konular", "DersID", "dbo.Dersler");
            DropIndex("dbo.KonuVideo", new[] { "KonuID" });
            DropIndex("dbo.Konular", new[] { "DersID" });
            DropTable("dbo.KonuVideo");
            DropTable("dbo.Konular");
            DropTable("dbo.Dersler");
        }
    }
}
