﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebProgramlamaMatematikciyiz.Models
{
    [Table("Konular")]

    public class Konuanlatim
    {
        [Key]
        public int KonuID { get; set; }
        [Required, MaxLength(200)]
        public string KonuAdi { get; set; }
        public int DersID { get; set; }
        [ForeignKey("DersID")]
        public Dersler Dersler { get; set; }
        public virtual List<KonuVideo> Konuvideos { get; set; }
    }
}