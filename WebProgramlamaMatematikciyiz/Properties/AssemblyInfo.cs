﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Bir derlemeyle ilgili Genel Bilgiler şu yollarla denetlenir:
// denetlenir. Bir derlemeyle ilişkilendirilmiş bilgileri değiştirmek için bu
// öznitelik değerlerini değiştirin.
[assembly: AssemblyTitle("WebProgramlamaMatematikciyiz")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("WebProgramlamaMatematikciyiz")]
[assembly: AssemblyCopyright("Telif hakkı ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible özelliğinin false olarak ayarlanması bu derlemedeki türleri COM bileşenlerine
//COM görünmez kılar.  Bu derlemedeki bir türe şu kaynaktan erişmeniz gerekiyorsa:
// COM'dan erişmeniz gerekiyorsa, o türde ComVisible özniteliğini true olarak ayarlayın.
[assembly: ComVisible(false)]

// Bu proje COM'a açılmışsa aşağıdaki GUID typelib'in ID'si içindir
[assembly: Guid("79ddb21e-f958-40b3-808f-983e8c8772c5")]

// Bir derlemenin sürüm bilgisi aşağıdaki dört değerden oluşur:
//
//      Ana Sürüm
//      İkincil Sürüm
//      Yapı Numarası
//      Düzeltme
//
// Tüm değerleri belirtebilir veya Gözden Geçirme ve Yapı Numaralarını varsayılan değerlerine döndürebilirsiniz
// gösterildiği gibi '*' ile varsayılan değer atayabilirsiniz:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
